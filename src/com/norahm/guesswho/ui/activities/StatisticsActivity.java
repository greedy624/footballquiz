package com.norahm.guesswho.ui.activities;

import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.norahm.guesswho.R;
import com.norahm.guesswho.utils.UserData;

public class StatisticsActivity extends SherlockActivity {

	UserData userData = UserData.getInstance();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_statistics);

		userData.init(StatisticsActivity.this);

		((TextView) findViewById(R.id.statsPointsName)).append(Integer
				.toString(userData.getPointsLevelname()));

		((TextView) findViewById(R.id.statsPointsPosition)).append(Integer
				.toString(userData.getPointsLevelpos()));
		
		((TextView) findViewById(R.id.statsPointsTeam)).append(Integer
				.toString(userData.getPointsLevelteam()));
		
		((ProgressBar) findViewById(R.id.ProgressBarname)).setMax(61);
		((ProgressBar) findViewById(R.id.ProgressBarposition)).setMax(61);
		((ProgressBar) findViewById(R.id.ProgressBarteam)).setMax(32);

		((ProgressBar) findViewById(R.id.ProgressBarname)).setProgress(userData.getMaxUnlockedLevelname());

		((ProgressBar) findViewById(R.id.ProgressBarposition)).setProgress(userData.getMaxUnlockedLevelpos());

		((ProgressBar) findViewById(R.id.ProgressBarteam)).setProgress(userData.getMaxUnlockedLevelteam());

	}
}
