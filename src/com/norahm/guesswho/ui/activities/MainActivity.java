package com.norahm.guesswho.ui.activities;

import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.MenuItem.OnMenuItemClickListener;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.norahm.guesswho.R;
import com.norahm.guesswho.model.AppRater;
import com.norahm.guesswho.services.CommunicateService;
import com.norahm.guesswho.utils.UserData;

public class MainActivity extends SherlockActivity {

	UserData userData = UserData.getInstance();
	private CommunicateService imService;
	private EasyTracker easyTracker = null;
	private String load_Game;
	private ServiceConnection mConnection = new ServiceConnection() {
		public void onServiceConnected(ComponentName className, IBinder service) {
			// This is called when the connection with the service has been
			// established, giving us the service object we can use to
			// interact with the service. Because we have bound to a explicit
			// service that we know is running in our own process, we can
			// cast its IBinder to a concrete class and directly access it.
			imService = ((CommunicateService.IMBinder) service).getService();
			//
			if (imService.isUserAuthenticated() == true) {
				Intent i = new Intent(MainActivity.this, PlayNameActivity.class);
				startActivity(i);
				MainActivity.this.finish();
			}
		}

		public void onServiceDisconnected(ComponentName className) {
			// This is called when the connection with the service has been
			// unexpectedly disconnected -- that is, its process crashed.
			// Because it is running in our same process, we should never
			// see this happen.
			imService = null;
			Toast.makeText(MainActivity.this, "The service has disconnected",
					Toast.LENGTH_SHORT).show();
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		startService(new Intent(MainActivity.this, CommunicateService.class));

		easyTracker = EasyTracker.getInstance(MainActivity.this);

		load_Game = getResources().getString(R.string.loading_game);

		// Get a Tracker (should auto-report)
		Tracker t = ((MyApplication) getApplication())
				.getTracker(MyApplication.TrackerName.APP_TRACKER);

		t.enableAdvertisingIdCollection(true);

		findViewById(R.id.playbyname).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				final LoadTask loadTask = new LoadTask();
				loadTask.execute();

				Intent Levelintent = new Intent(getBaseContext(),
						DisplayLevelActivity.class);
				Levelintent.putExtra("typeGame", 1);
				startActivity(Levelintent);
				easyTracker.send(MapBuilder.createEvent("PlayType",
						"PlayByName", "PlayByName/id", null).build());

			}
		});

		findViewById(R.id.playbypos).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				final LoadTask loadTask = new LoadTask();
				loadTask.execute();

				Intent Levelintent = new Intent(getBaseContext(),
						DisplayLevelActivity.class);
				Levelintent.putExtra("typeGame", 0);
				startActivity(Levelintent);
				easyTracker.send(MapBuilder.createEvent("PlayType",
						"PlayByPos", "PlayByPos/id", null).build());
			}
		});

		findViewById(R.id.playbyteam).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				final LoadTask loadTask = new LoadTask();
				loadTask.execute();

				Intent Levelintent = new Intent(getBaseContext(),
						DisplayLevelActivity.class);
				Levelintent.putExtra("typeGame", 2);
				startActivity(Levelintent);
				easyTracker.send(MapBuilder.createEvent("PlayType",
						"PlayByPos", "PlayByPos/id", null).build());
			}
		});
		
		findViewById(R.id.gamestats).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent StatsIntent = new Intent(getBaseContext(),
						StatisticsActivity.class);
				startActivity(StatsIntent);
			}
		});

	}

	private class LoadTask extends AsyncTask<Void, Integer, Boolean> {

		ProgressDialog pdLoading = new ProgressDialog(MainActivity.this);

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			// this method will be running on UI thread
			pdLoading.setMessage("\t" + load_Game);
			pdLoading.show();
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			Boolean res = false;

			/*
			 * while(imService==null) { if(imService!=null) break; }
			 */
			res = imService.LoadPlayersData();

			userData.init(MainActivity.this);

			return res;
		}

		@Override
		protected void onPostExecute(Boolean result) {

			// this method will be running on UI thread

			pdLoading.dismiss();

			if (!result)
				Toast.makeText(MainActivity.this, "Error Loading Game!",
						Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	protected void onPause() {
		unbindService(mConnection);
		//Log.v("Loggin", "onPause");
		super.onPause();
	}

	@Override
	protected void onRestart() {

		//Log.v("Loggin", "onRestart");
		super.onRestart();
	}

	@Override
	protected void onResume() {
		bindService(new Intent(MainActivity.this, CommunicateService.class),
				mConnection, Context.BIND_AUTO_CREATE);
	//	Log.v("Loggin", "onResume");

		super.onResume();
	}

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this);
		GoogleAnalytics.getInstance(this).reportActivityStart(this);
		AppRater.app_launched(this);
	}

	@Override
	public void onStop() {
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this);
		GoogleAnalytics.getInstance(this).reportActivityStop(this);
	}
	
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		// Third Menu Button
		menu.add("About")
				.setOnMenuItemClickListener(this.AboutButtonClickListener)
				.setIcon(R.drawable.info) // Set the menu icon
				.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);

		return super.onCreateOptionsMenu(menu);
	}
	
	OnMenuItemClickListener AboutButtonClickListener = new OnMenuItemClickListener() {

		public boolean onMenuItemClick(MenuItem item) {

			imService.About();

			return false;
		}
	};
}
