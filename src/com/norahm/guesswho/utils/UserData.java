package com.norahm.guesswho.utils;

import java.lang.reflect.Type;
import java.util.ArrayList;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class UserData {

	private static UserData INSTANCE;

	private static final String PREFS_NAME = "GAME_USERDATA";

	/*
	 * These keys will tell the shared preferences editor which data we're
	 * trying to access
	 */

	private static final String UNLOCKED_LEVEL_KEY_NAME = "unlocked_level_key_name";
	private static final String UNLOCKED_LEVEL_KEY_POS = "unlocked_level_key_pos";
	private static final String UNLOCKED_LEVEL_KEY_TEAM = "unlocked_level_key_team";

	private static final String POINTS_KEY_TEAM = "points_key_team";
	private static final String POINTS_KEY_NAME = "points_key_name";
	private static final String POINTS_KEY_POS = "points_key_pos";

	private static final String SOUND_KEY = "soundKey";
	private static final String EDITED[] = { "ed1", "ed2", "ed3", "ed4", "ed5" };

	private static final String NUMOFTRIES_LEVEL_KEY_NAME = "numoftries_level_key_name";
	private static final String NUMOFTRIES_LEVEL_KEY_POS = "numoftries_level_key_pos";
	private static final String NUMOFTRIES_LEVEL_KEY_TEAM = "numoftries_level_key_team";
	/*
	 * Create our shared preferences object and editor which will be used to
	 * save and load data
	 */

	private SharedPreferences mSettings;
	private SharedPreferences.Editor mEditor;

	// keep track of max unlocked level
	public int mUnlockedLevelname = 1;
	public int mUnlockedLevelpos = 1;
	public int mUnlockedLevelteam = 1;

	public int mpointsLevelname = 1;
	public int mpointsLevelpos = 1;
	public int mpointsLevelteam = 1;

	private ArrayList<Integer> mNumOfTriesname = new ArrayList<Integer>();
	private ArrayList<Integer> mNumOfTriespos = new ArrayList<Integer>();
	private ArrayList<Integer> mNumOfTriesteam = new ArrayList<Integer>();
	public String mNumOfTriesName = "";
	public String mNumOfTriesPos = "";
	public String mNumOfTriesTeam = "";

	public boolean mEditedLevel[] = { false, false, false, false, false };

	// keep track of whether or not not sound is enabled
	private boolean mSoundEnabled;

	UserData() {

	}

	public synchronized static UserData getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new UserData();
			// Log.v("", "Creates a new instance of UserData");
		}
		// Log.v("", "returns the instance of UserData");
		return INSTANCE;
	}

	public synchronized void init(Context pContext) {
		if (mSettings == null) {

			// use static Context.MODE_PRIVATE, instead pContext.MODE_PRIVATE
			// as an argument in getSharedPreferences
			mSettings = pContext.getSharedPreferences(PREFS_NAME,
					Context.MODE_PRIVATE);

			mEditor = mSettings.edit();

			// same key used for level, highscore, and editedlevel
			// which means all these variables will have the same value
			// determined by "GAME_USERDATA" key
			mUnlockedLevelname = mSettings.getInt(UNLOCKED_LEVEL_KEY_NAME, 1);
			mUnlockedLevelpos = mSettings.getInt(UNLOCKED_LEVEL_KEY_POS, 1);
			mUnlockedLevelteam = mSettings.getInt(UNLOCKED_LEVEL_KEY_TEAM, 1);

			mpointsLevelteam = mSettings.getInt(POINTS_KEY_TEAM, 5);
			mpointsLevelpos = mSettings.getInt(POINTS_KEY_POS, 5);
			mpointsLevelname = mSettings.getInt(POINTS_KEY_NAME, 5);

			mSoundEnabled = mSettings.getBoolean(SOUND_KEY, true);

			for (int i = 0; i < 5; i++)
				mEditedLevel[i] = mSettings.getBoolean(EDITED[i], false);

			Gson gson = new Gson();

			for (int i = 0; i < 61; i++) {
				mNumOfTriesname.add(0);
				mNumOfTriespos.add(0);
				if (i < 32)
					mNumOfTriesteam.add(0);
			}
			String json = gson.toJson(mNumOfTriesname);
			mNumOfTriesName = mSettings.getString(NUMOFTRIES_LEVEL_KEY_NAME,
					json);
			mNumOfTriesPos = mSettings
					.getString(NUMOFTRIES_LEVEL_KEY_POS, json);

			json = gson.toJson(mNumOfTriesteam);
			mNumOfTriesTeam = mSettings.getString(NUMOFTRIES_LEVEL_KEY_TEAM,
					json);

			// Log.v("", "Set up initial values for UserData "
			// + mUnlockedLevelname + " " + mUnlockedLevelpos + " "
			// + mSoundEnabled);
		}
	}

	public synchronized int getMaxUnlockedLevelname() {
		return mUnlockedLevelname;
	}

	public synchronized int getMaxUnlockedLevelpos() {
		return mUnlockedLevelpos;
	}

	public synchronized int getMaxUnlockedLevelteam() {
		return mUnlockedLevelteam;
	}

	public synchronized int getPointsLevelname() {
		return mpointsLevelname;
	}

	public synchronized int getPointsLevelpos() {
		return mpointsLevelpos;
	}

	public synchronized int getPointsLevelteam() {
		return mpointsLevelteam;
	}

	public synchronized void incrementPointsLevelname(int time, int numoftries) {

		int add_points = 1;
		if (time < 150) {
			add_points += (250 / time);
		} else {
			add_points += (150 / time);
		}
		mpointsLevelname += add_points;
		mEditor.putInt(POINTS_KEY_NAME, mpointsLevelname);
		mEditor.commit();
	}

	public synchronized void incrementPointsLevelpos(int time, int numoftries) {
		int add_points = 1;
		if (time < 150) {
			add_points += (250 / time);
		} else {
			add_points += (150 / time);
		}
		mpointsLevelpos += add_points;
		mEditor.putInt(POINTS_KEY_POS, mpointsLevelpos);
		mEditor.commit();
	}

	public synchronized void incrementPointsLevelteam(int time, int numoftries) {
		int add_points = 1;
		if (time < 150) {
			add_points += (650 / time);
		} else {
			add_points += (400 / time);
		}

		mpointsLevelteam += add_points;
		mEditor.putInt(POINTS_KEY_TEAM, mpointsLevelteam);
		mEditor.commit();
	}

	public synchronized void decrementPointsLevelname(int points) {

		mpointsLevelname -= points;

		if (mpointsLevelname < 0)
			mpointsLevelname = 0;

		mEditor.putInt(POINTS_KEY_NAME, mpointsLevelname);
		mEditor.commit();
	}

	public synchronized void decrementPointsLevelpos(int points) {

		mpointsLevelpos -= points;

		if (mpointsLevelpos < 0)
			mpointsLevelpos = 0;

		mEditor.putInt(POINTS_KEY_POS, mpointsLevelpos);
		mEditor.commit();
	}

	public synchronized void decrementPointsLevelteam(int points) {

		mpointsLevelteam -= points;

		if (mpointsLevelteam < 0)
			mpointsLevelteam = 0;

		mEditor.putInt(POINTS_KEY_TEAM, mpointsLevelteam);
		mEditor.commit();
	}

	public synchronized int getNumofTriesPos(int level) {

		Gson gson = new Gson();
		String json = mSettings.getString(NUMOFTRIES_LEVEL_KEY_POS,
				mNumOfTriesPos);
		Type type = new TypeToken<ArrayList<Integer>>() {
		}.getType();
		mNumOfTriespos = gson.fromJson(json, type);

		return mNumOfTriespos.get(level);
	}

	public synchronized int getNumofTriesName(int level) {
		Gson gson = new Gson();
		String json = mSettings.getString(NUMOFTRIES_LEVEL_KEY_NAME,
				mNumOfTriesName);
		Type type = new TypeToken<ArrayList<Integer>>() {
		}.getType();
		mNumOfTriesname = gson.fromJson(json, type);

		return mNumOfTriesname.get(level);
	}

	public synchronized int getNumofTriesTeam(int level) {
		Gson gson = new Gson();
		String json = mSettings.getString(NUMOFTRIES_LEVEL_KEY_TEAM,
				mNumOfTriesTeam);
		Type type = new TypeToken<ArrayList<Integer>>() {
		}.getType();
		mNumOfTriesteam = gson.fromJson(json, type);

		return mNumOfTriesteam.get(level);
	}

	public synchronized boolean isSoundMuted() {
		return mSoundEnabled;
	}

	public synchronized boolean getEdited(int i) {
		// Log.v("", "mEditedLevel before increment " + mEditedLevel[i]);
		return mEditedLevel[i];
	}

	public synchronized void unlockNextLevelNAME() {
		mUnlockedLevelname++;
		mEditor.putInt(UNLOCKED_LEVEL_KEY_NAME, mUnlockedLevelname);
		mEditor.commit();
	}

	public synchronized void unlockNextLevelPOS() {
		mUnlockedLevelpos++;
		mEditor.putInt(UNLOCKED_LEVEL_KEY_POS, mUnlockedLevelpos);
		mEditor.commit();
	}

	public synchronized void unlockNextLevelTEAM() {
		mUnlockedLevelteam++;
		mEditor.putInt(UNLOCKED_LEVEL_KEY_TEAM, mUnlockedLevelteam);
		mEditor.commit();
	}

	public synchronized void incrementNumofTriesPos(int level) {
		Gson gson = new Gson();
		String json = mSettings.getString(NUMOFTRIES_LEVEL_KEY_POS,
				mNumOfTriesPos);
		Type type = new TypeToken<ArrayList<Integer>>() {
		}.getType();
		mNumOfTriespos = gson.fromJson(json, type);
		int currenTries = mNumOfTriespos.get(level);
		currenTries++;
		mNumOfTriespos.set(level, currenTries);

		json = gson.toJson(mNumOfTriespos);
		mEditor.putString(NUMOFTRIES_LEVEL_KEY_POS, json);
		mEditor.commit();
	}

	public synchronized void incrementNumofTriesName(int level) {
		Gson gson = new Gson();
		String json = mSettings.getString(NUMOFTRIES_LEVEL_KEY_NAME,
				mNumOfTriesName);
		Type type = new TypeToken<ArrayList<Integer>>() {
		}.getType();
		mNumOfTriesname = gson.fromJson(json, type);
		int currenTries = mNumOfTriesname.get(level);
		currenTries++;
		mNumOfTriesname.set(level, currenTries);

		json = gson.toJson(mNumOfTriesname);
		mEditor.putString(NUMOFTRIES_LEVEL_KEY_NAME, json);
		mEditor.commit();
	}

	public synchronized void incrementNumofTriesTeam(int level) {
		Gson gson = new Gson();
		String json = mSettings.getString(NUMOFTRIES_LEVEL_KEY_TEAM,
				mNumOfTriesTeam);
		Type type = new TypeToken<ArrayList<Integer>>() {
		}.getType();
		mNumOfTriesteam = gson.fromJson(json, type);
		int currenTries = mNumOfTriesteam.get(level);
		currenTries++;
		mNumOfTriesteam.set(level, currenTries);

		json = gson.toJson(mNumOfTriesteam);
		mEditor.putString(NUMOFTRIES_LEVEL_KEY_TEAM, json);
		mEditor.commit();
	}

	public synchronized void setSoundMuted(final boolean pEnableSound) {
		mSoundEnabled = pEnableSound;
		mEditor.putBoolean(SOUND_KEY, mSoundEnabled);
		mEditor.commit();
	}

	public synchronized void setEdited(int i, final boolean newEdited) {
		mEditedLevel[i] = newEdited;
		// Log.v("", "mEditedLevel is " + mEditedLevel[i]);
		mEditor.putBoolean(EDITED[i], mEditedLevel[i]);
		mEditor.commit();
	}
}
