package com.norahm.guesswho.model;

public class CastrolPlayer {

	public CastrolPlayer(int id, String pic_id, String name, String team,
			String position, String index) {

		this.id = id;
		this.pic_id = pic_id;
		this.Name = name;
		this.Team = team;
		this.Position = position;
		this.index = index;
	}

	public CastrolPlayer() {
		// TODO Auto-generated constructor stub
	}

	public int id;
	public String pic_id;
	public String Name;
	public String Team;
	public String Position;
	public String index;
}
