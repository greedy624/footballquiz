package com.norahm.guesswho.services;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import android.app.Dialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.norahm.guesswho.model.AppRater;
import com.norahm.guesswho.model.CastrolPlayer;

public class CommunicateService extends Service {

	private final IBinder mBinder = new IMBinder();
	public ArrayList<CastrolPlayer> Players = new ArrayList<CastrolPlayer>();
	public Map<String, ArrayList<CastrolPlayer>> playersByTeam = new HashMap<String, ArrayList<CastrolPlayer>>();
	public Map<String, Integer> teamsCount = new HashMap<String, Integer>();
	public String[] teams;
	public Boolean isDataLoaded = false;
	private boolean authenticatedUser = false;

	public class IMBinder extends Binder {
		public CommunicateService getService() {
			return CommunicateService.this;
		}

	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return mBinder;
	}

	@Override
	public void onDestroy() {
		// Log.i("Communication service is being destroyed", "...");
		super.onDestroy();
	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		// Log.v("Service", "Created");
	}

	public boolean isUserAuthenticated() {
		return authenticatedUser;
	}

	public Boolean LoadPlayersData() {

		Scanner sc = null;
		ArrayList<CastrolPlayer> temp;
		// BufferedReader playersinput = null;
		try {
			File f = new File(getCacheDir() + "/castrolRank.dat");
			if (!f.exists())
				try {

					InputStream is = getAssets().open("castrolRank.dat");
					int size = is.available();
					byte[] buffer = new byte[size];
					is.read(buffer);
					is.close();

					FileOutputStream fos = new FileOutputStream(f);
					fos.write(buffer);
					fos.close();
				} catch (Exception e) {
					throw new RuntimeException(e);
				}

			// playersinput = new BufferedReader(new FileReader(f.getPath()));

			sc = new Scanner(f);
			String line;
			String[] details;
			String pic_id, Name, Team, Position, index;
			int id;
			int count = 0;

			while (sc.hasNextLine()) {
				line = sc.nextLine();
				details = line.split(";");
				id = count++;
				pic_id = details[0];
				Name = details[1];
				Team = details[2];
				Position = details[3];
				index = details[4];

				// int age = Integer.parseInt(details[2]);
				CastrolPlayer p = new CastrolPlayer(id, pic_id, Name, Team,
						Position, index);

				if (teamsCount.containsKey(Team)) {
					int teamcount = teamsCount.get(Team);
					teamcount++;
					teamsCount.remove(Team);
					teamsCount.put(Team, teamcount);

					temp = playersByTeam.get(Team);

					temp.add(p);
					playersByTeam.remove(Team);
					playersByTeam.put(Team, temp);

				} else {
					temp = new ArrayList<CastrolPlayer>();
					temp.add(p);
					teamsCount.put(Team, 1);
					playersByTeam.put(Team, temp);
				}
				Players.add(p);
			}

		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
			return false;

		} finally {
			Set<String> tempteams = teamsCount.keySet();
			teams = tempteams.toArray(new String[tempteams.size()]);
			if (sc != null)
				sc.close();
		}
		isDataLoaded = true;
		return true;
	}

	public void showRateDialog(final Context mContext, String level) {
		final Dialog dialog = new Dialog(mContext);
		final String description = "I reached level " + level + " in "
				+ AppRater.APP_TITLE + "\n"
				+ " Can you do better? \ntry it here ";
		dialog.setTitle("Share Result ");

		LinearLayout ll = new LinearLayout(mContext);
		ll.setOrientation(LinearLayout.VERTICAL);

		TextView tv = new TextView(mContext);
		tv.setText("Congrats You unlocked level " + level
				+ " do you want to Share this on Facebook");
		tv.setWidth(340);
		tv.setPadding(4, 0, 4, 10);
		ll.addView(tv);

		Button b1 = new Button(mContext);
		b1.setText("Share Result");
		b1.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				Intent intent = mContext.getPackageManager()
						.getLaunchIntentForPackage("com.facebook.katana");
				if (intent != null) {
					// The application exists
					Intent shareIntent = new Intent(Intent.ACTION_SEND);
					// shareIntent.setPackage("com.facebook.katana");

					shareIntent.putExtra(android.content.Intent.EXTRA_TITLE,
							"I completed a new level in");
					shareIntent.putExtra(Intent.EXTRA_TEXT, description);
					mContext.startActivity(Intent.createChooser(shareIntent,
							"Share using"));

					// Start the specific social application
					// mContext.startActivity(shareIntent);
				}

				dialog.dismiss();
			}
		});
		ll.addView(b1);

		Button b2 = new Button(mContext);
		b2.setText("No Thanks!");
		b2.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				dialog.dismiss();
			}
		});
		ll.addView(b2);

		dialog.setContentView(ll);
		dialog.show();
	}

	public void About() {
		Intent intent = new Intent(getBaseContext(),
				com.norahm.guesswho.ui.activities.AboutActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.putExtra("type", "about");
		startActivity(intent);
	}
}
